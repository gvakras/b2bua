package eu.voiceweb.sip.proxy;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sip.*;
import javax.sip.address.Address;
import javax.sip.address.AddressFactory;
import javax.sip.address.SipURI;
import javax.sip.header.*;
import javax.sip.message.Request;
import javax.sip.message.Response;
import java.util.Hashtable;
import java.util.Iterator;

public class Proxy implements SipListener {

    private SipProvider inviteServerTxProvider;
    private Hashtable<Integer, ClientTransaction> clientTxTable = new Hashtable<>();

    private final static String host = "192.168.110.45";
    private final static String transport = "udp";

    private static Logger logger = Logger.getLogger(Proxy.class);

    @Autowired
    private AddressFactory addressFactory;

    @Autowired
    private HeaderFactory headerFactory;

    private void sendTo(ServerTransaction serverTx, Request request, SipProvider sipProvider, int targetPort) throws Exception {
        Request clonedRequest = (Request) request.clone();

        SipURI sipUri = addressFactory.createSipURI("124", "192.168.100.166");
        sipUri.setPort(targetPort);
        sipUri.setLrParam();

        // add route header
        Address address = addressFactory.createAddress("client1", sipUri);
        RouteHeader routeHeader = headerFactory.createRouteHeader(address);
        clonedRequest.removeFirst(RouteHeader.NAME);
        clonedRequest.addLast(routeHeader);

        // add via header
        ViaHeader viaHeader = headerFactory.createViaHeader(host, 5070, transport, null);
        clonedRequest.addFirst(viaHeader);

        // add record route header
        SipURI proxyUri = addressFactory.createSipURI("proxy", "192.168.110.45");
        proxyUri.setPort(5070);
        proxyUri.setLrParam();
        Address proxyAddress = addressFactory.createAddress("proxy", proxyUri);
        RecordRouteHeader recordRoute = headerFactory.createRecordRouteHeader(proxyAddress);
        clonedRequest.addHeader(recordRoute);

        // add a Content-Length header field if necessary
        ContentLengthHeader contentLengthHeader = (ContentLengthHeader) clonedRequest.getHeader(ContentLengthHeader.NAME);
        if (contentLengthHeader == null) {
            byte[] contentData = request.getRawContent();
            contentLengthHeader = headerFactory.createContentLengthHeader(contentData == null ? 0 : contentData.length);
            clonedRequest.setContentLength(contentLengthHeader);
        }

        ClientTransaction clientTx = sipProvider.getNewClientTransaction(clonedRequest);
        clientTx.setApplicationData(serverTx);
        clientTxTable.put(targetPort, clientTx);
        clientTx.sendRequest();
    }

    public void processRequest(RequestEvent requestEvent) {
        try {
            Request request = requestEvent.getRequest();
            SipProvider sipProvider = (SipProvider) requestEvent.getSource();
            inviteServerTxProvider = sipProvider;
            if (request.getMethod().equals(Request.INVITE)) {
                ServerTransaction serverTx = null;
                if (requestEvent.getServerTransaction() == null) {
                    serverTx = sipProvider.getNewServerTransaction(request);
                }
                sendTo(serverTx, request, sipProvider, 5070);
            } else {
                // Remove the topmost route header
                // The route header will make sure it gets to the right place.
                logger.info("proxy: Got a request " + request.getMethod());
                Request newRequest = (Request) request.clone();
                newRequest.removeFirst(RouteHeader.NAME);
                sipProvider.sendRequest(newRequest);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void processResponse(ResponseEvent responseEvent) {
        Response response = responseEvent.getResponse();
        ClientTransaction ctx = responseEvent.getClientTransaction();
        CSeqHeader cseq = (CSeqHeader) response.getHeader(CSeqHeader.NAME);

        try {

            // stateful proxy MUST NOT forward 100 Trying
            if (response.getStatusCode() == 100) {
                return;
            }

            if (cseq.getMethod().equals(Request.INVITE)) {

                if (ctx != null) {
                    ServerTransaction tx = (ServerTransaction) ctx.getApplicationData();
                    Response newResponse = (Response) response.clone();
                    newResponse.removeFirst(ViaHeader.NAME);
                    tx.sendResponse(newResponse);
                } else {
                    // Client tx has already terminated but the UA is retransmitting just forward the response statelessly.
                    Response newResponse = (Response) response.clone();
                    newResponse.removeFirst(ViaHeader.NAME);
                    inviteServerTxProvider.sendResponse(newResponse);
                }
            } else {
                // this is the OK for the cancel.
                logger.info("Got a non-invite response " + response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void processTimeout(TimeoutEvent timeoutEvent) {
        logger.error("Timeout occured");
    }

    public void processIOException(IOExceptionEvent exceptionEvent) {
        logger.info("IOException occured");
    }

    public void processTransactionTerminated(TransactionTerminatedEvent transactionTerminatedEvent) {
        logger.info("Transaction terminated event occured -- cleaning up");
        if (!transactionTerminatedEvent.isServerTransaction()) {
            ClientTransaction ct = transactionTerminatedEvent.getClientTransaction();
            for (Iterator<ClientTransaction> it = this.clientTxTable.values().iterator(); it.hasNext(); ) {
                if (it.next().equals(ct)) {
                    it.remove();
                }
            }
        } else {
            logger.info("Server tx terminated! ");
        }
    }

    public void processDialogTerminated(DialogTerminatedEvent dialogTerminatedEvent) {
    }

}