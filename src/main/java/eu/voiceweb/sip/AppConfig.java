package eu.voiceweb.sip;

import eu.voiceweb.sip.proxy.Proxy;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.inject.Inject;
import javax.sip.*;
import javax.sip.address.AddressFactory;
import javax.sip.header.HeaderFactory;
import javax.sip.message.MessageFactory;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Properties;
import java.util.TooManyListenersException;

@Configuration
@ComponentScan
public class AppConfig {

    private static final SipFactory sipFactory = SipFactory.getInstance();

    @Inject
    private SipStack sipStack;

    @Inject
    private Proxy proxy;


    public static void main(String[] args) {
        sipFactory.setPathName("gov.nist");
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    }

    @Bean
    public Proxy createSipProxy() throws SipException, TooManyListenersException, InvalidArgumentException {
        Proxy proxy = new Proxy();
        ListeningPoint lp = sipStack.createListeningPoint("192.168.110.45", 5070, "udp");
        SipProvider sipProvider = sipStack.createSipProvider(lp);
        sipProvider.addListeningPoint(lp);
        sipProvider.addSipListener(proxy);
        return proxy;
    }

    @Bean
    public SipStack createSipStack() throws PeerUnavailableException, IOException {
        Properties properties = new Properties();
        properties.load(Proxy.class.getResourceAsStream("/sip.properties"));
        properties.setProperty("javax.sip.AUTOMATIC_DIALOG_SUPPORT", "off");
        properties.setProperty("javax.sip.STACK_NAME", "Proxy");
        return sipFactory.createSipStack(properties);
    }

    @Bean
    public AddressFactory createAddressFactory() throws PeerUnavailableException {
        return sipFactory.createAddressFactory();
    }

    @Bean
    public HeaderFactory createHeaderFactory() throws PeerUnavailableException {
        return sipFactory.createHeaderFactory();
    }

    @Bean
    public MessageFactory createMessageFactory() throws PeerUnavailableException {
        return sipFactory.createMessageFactory();
    }

}